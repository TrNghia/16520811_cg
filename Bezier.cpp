#include "Bezier.h"
#include <iostream>
#include "Circle.h"
using namespace std;

void DrawCurve2(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3)
{
	for (float i = 0; i <= 1; i += 0.0001)
	{
		int x = (1 - i)*((1 - i)*p1.x + i*p2.x) + i*((1 - i)*p2.x + i*p3.x);
		int y = (1 - i)*((1 - i)*p1.y + i*p2.y) + i*((1 - i)*p2.y + i*p3.y);
		SDL_RenderDrawPoint(ren,x,y);
	}
	MidpointDrawCircle(p1.x, p1.y, 3, ren);
	MidpointDrawCircle(p2.x, p2.y, 3, ren);
	MidpointDrawCircle(p3.x, p3.y, 3, ren);


}
void DrawCurve3(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3, Vector2D p4)
{
	for (float i = 0; i <= 1; i += 0.0001)
	{
		int x = (1 - i)*((1 - i)*(1 - i)*p1.x + 2 * i*(1 - i) *p2.x + i*i*p3.x) + i*((1 - i)*(1 - i)*p2.x + 2 * i*(1 - i)*p3.x + i*i*p4.x);
		int y = (1 - i)*((1 - i)*(1 - i)*p1.y + 2 * i*(1 - i) *p2.y + i*i*p3.y) + i*((1 - i)*(1 - i)*p2.y + 2 * i*(1 - i)*p3.y + i*i*p4.y);
		SDL_RenderDrawPoint(ren, x, y);
	}
	MidpointDrawCircle(p1.x, p1.y, 3, ren);
	MidpointDrawCircle(p2.x, p2.y, 3, ren);
	MidpointDrawCircle(p3.x, p3.y, 3, ren);
	MidpointDrawCircle(p4.x, p4.y, 3, ren);


}


