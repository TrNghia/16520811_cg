#include <iostream>
#include <SDL.h>
#include "Bezier.h"
#include "FillColor.h"

using namespace std;

const int WIDTH = 800;
const int HEIGHT = 1000;

SDL_Event event;


int main(int, char**) {
	//First we need to start up SDL, and make sure it went ok
	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
		std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	SDL_Window *win = SDL_CreateWindow("Hello World!", 0, 0, WIDTH, HEIGHT, SDL_WINDOW_SHOWN);
	//Make sure creating our window went ok
	if (win == NULL) {
		std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	//Create a renderer that will draw to the window, -1 specifies that we want to load whichever
	//DON'T FORGET CHANGE THIS LINE IN YOUR SOURCE ----->>>>> SDL_RENDERER_SOFTWARE

	SDL_Renderer *ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_SOFTWARE);
	if (ren == NULL) {
		SDL_DestroyWindow(win);
		std::cout << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
		SDL_Quit();
		return 1;
	}

	SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
	SDL_RenderClear(ren);

	//YOU CAN INSERT CODE FOR TESTING HERE
	SDL_SetRenderDrawColor(ren, 0, 0, 255, 255);
	Vector2D p1(100, 150), p2(200, 200), p3(300, 400),p4(600,200),p;
	//DrawCurve3(ren, p1, p2, p3,p4);
	SDL_Color fillColor;
	fillColor.a = 0;
	fillColor.b = fillColor.g = fillColor.r = 200;
	SDL_SetRenderDrawColor(ren, 0, 0, 255, 255);
	TriangleFill(p1, p2, p3, ren, fillColor);
	SDL_RenderPresent(ren);
	/*SDL_SetRenderDrawColor(ren, 0, 0, 253, 255);
	RectangleFill(p1, p2, ren, fillColor);
	SDL_RenderPresent(ren);
	SDL_SetRenderDrawColor(ren, 0, 0, 253, 255);
	CircleFill(100, 200, 50, ren, fillColor);
	SDL_RenderPresent(ren);
*/


	//Take a quick break after all that hard work
	//Quit if happen QUIT event
	SDL_RenderPresent;
	bool running = true;

	while (running)
	{
		//If there's events to handle
		if (SDL_PollEvent(&event))
		{

			//If the user has Xed out the window

			if (event.type == SDL_QUIT)
			{
				//Quit the program
				running = false;
			}
				if (event.type == SDL_MOUSEBUTTONDOWN)
				{
					int x, y;
					SDL_GetMouseState(&x, &y);
					if ((x - p1.x)*(x - p1.x) + (y - p1.y)*(y - p1.y) <=  9)
						p = p1;
					else
						if ((x - p2.x)*(x - p2.x) + (y - p2.y)*(y - p2.y) <= 9)
							p = p2;
					else
					if ((x - p3.x)*(x - p3.x) + (y - p3.y)*(y - p3.y) <= 9)
						p = p3;
					else
					if ((x - p4.x)*(x - p4.x) + (y - p4.y)*(y - p4.y) <= 9)
						p = p4;
				}
				if (event.type == SDL_MOUSEMOTION)
				{
					int x, y;
					SDL_GetMouseState(&x, &y);
						if (p.x == p1.x && p.y == p1.y)
							p1.x = x, p1.y = y,p.x=x,p.y=y;
						else
						if (p.x == p2.x && p.y == p2.y)
							p2.x = x, p2.y = y, p.x = x, p.y = y;
						else
						if (p.x == p3.x && p.y == p3.y)
							p3.x = x, p3.y = y, p.x = x, p.y = y;
						else
						if (p.x == p4.x && p.y == p4.y)
							p4.x = x, p4.y = y, p.x = x, p.y = y;

				}
				if (event.type == SDL_MOUSEBUTTONUP)
				{
					p = NULL;
				}
			SDL_SetRenderDrawColor(ren,0, 0, 0, 253);
			SDL_RenderClear(ren);
			SDL_SetRenderDrawColor(ren,0, 100, 0, 255);
			DrawCurve3(ren, p1, p2, p3,p4);
			SDL_RenderPresent(ren);
	
		}

	}

	SDL_DestroyRenderer(ren);
	SDL_DestroyWindow(win);
	SDL_Quit();

	return 0;
}
