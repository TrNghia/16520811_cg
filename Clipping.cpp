#include "Clipping.h"

RECT CreateWindow(int l, int r, int t, int b)
{
	RECT rect;
	rect.Left = l;
	rect.Right = r;
	rect.Top = t;
	rect.Bottom = b;

	return rect;
}

CODE Encode(RECT r, Vector2D P)
{
	CODE c = 0;
	if (P.x < r.Left)
		c = c | LEFT;
	if (P.x > r.Right)
		c = c | RIGHT;
	if (P.y < r.Top)
		c = c | TOP;
	if (P.y > r.Bottom)
		c = c | BOTTOM;
	return c;
}

int CheckCase(int c1, int c2)
{
	if (c1 == 0 && c2 == 0)
		return 1;
	if (c1 != 0 && c2 != 0 && c1&c2 != 0)
		return 2;
	return 3;
}

int CohenSutherland(RECT r, Vector2D P1, Vector2D P2, Vector2D &Q1, Vector2D &Q2)
{
	int c1 = Encode(r, P1);
	int c2 = Encode(r, P2);
	int check;
	check = CheckCase(c1, c2);
	if (check == 2)
		return 0;

	while (check == 3)
	{
		ClippingCohenSutherland(r, P1, P2);
		c1 = Encode(r, P1);
		c2 = Encode(r, P2);
		check = CheckCase(c1, c2);
	}
	
	Q1 = P1;
	Q2 = P2;
	return 1;
}

void ClippingCohenSutherland(RECT r, Vector2D &P1, Vector2D &P2)
{
	}

int SolveNonLinearEquation(int p, int q, float &t1, float &t2)
{
	if (p == 0)
	{
		if (q < 0)
			return 0;
		return 1;
	}
	if (p > 0)
	{
		float t = (float)q / p;
		if (t < t1)
			return 0;
		if (t2 < t)
			return 1;
		t2 = t;
		return 1;
	}

	float t = (float)q / p;
	if (t<t1)
		return 1;
	if (t2<t)
		return 0;
	t1 = t;
	return 1;
}

int LiangBarsky(RECT r, Vector2D P1, Vector2D P2, Vector2D &Q1, Vector2D &Q2)
{
	float t1, t2;
	t1 = 0;
	t2 = 1;
	int Dx, Dy, x1, y1, x2, y2, xmin, ymin, xmax, ymax;
	x1 = P1.x; y1 = P1.y;
	x2 = P2.x; y2 = P2.y;
	Dx = x2 - x1; Dy = y2 - y1;
	xmin = r.Left; ymin = r.Top;
	xmax = r.Right; ymax = r.Bottom;
	if (SolveNonLinearEquation(-Dx, x1 - xmin, t1, t2)){
	if (SolveNonLinearEquation(Dx, xmax - x1, t1, t2)){
	if (SolveNonLinearEquation(-Dy, y1 - ymin, t1, t2)){
	if (SolveNonLinearEquation(Dy, ymax - y1, t1, t2))
		{
		Q1.x = x1 + t1*Dx;
		Q1.y = y1 + t1*Dy;
		Q2.x = x1 + t2*Dx;
		Q2.y = y1 + t2*Dy;
		}}}}
	return 0;

}
