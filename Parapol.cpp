#include "Parapol.h"
#include <math.h>

void Draw2Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	SDL_RenderDrawPoint(ren, xc + x, xc + y);
	SDL_RenderDrawPoint(ren, xc - x, xc + y);

	//draw 2 points

}
void BresenhamDrawParapolPositive(int xc, int yc, int A, SDL_Renderer *ren)
{
		int x = 0;
		int y = 0;
		float a = 1 / (2 * A);
		float p = a - 0.5;

		Draw2Points(xc, yc, x, y, ren);
		while ( A >x )
		{
			if (p > 0)
				p = p - 2 * x - 1;
			else
			{
				p = p + 2 * A - 1 - 2 * x;
				y++;
			}
			x++;
			Draw2Points(xc, yc, x, y, ren);
		}
		x = A;
		y = A / 2;
		int c, d;
		SDL_GetRendererOutputSize(ren, &c, &d);
		while (x>0 && y>0 && x<c && y<d)
		{
			if (p>=0)
			{
				p = p + 2 * sqrt(float(2 * A))*(sqrt(float(1+ y)) - sqrt(float(y))) - 2;
				x ++;
			}
			else
				p = p + 2 * sqrt(float(2 * A))*(sqrt(float(1+ y)) - sqrt(float(y)));
			y++;
			Draw2Points(xc, yc, x, y, ren);
		}
	}

void BresenhamDrawParapolNegative(int xc, int yc, int A, SDL_Renderer *ren)
{

}