#include "DrawPolygon.h"
#include "Line.h"
#include <iostream>
#include <math.h>
using namespace std;

void DrawEquilateralTriangle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[3];
	int y[3];
	float quay = 3.14 / 2;
	for (int i = 0; i < 3; i++)
	{
		x[i] = xc + int( R*cos(quay) + 0.5);
		y[i] = yc - int( R*sin(quay) + 0.5);
		quay = quay + (2 * quay) / 3;
	}
	for (int i = 0; i <= 2; i++)
	{
		SDL_RenderDrawPoint(ren, x[i], y[i]);
	}
	for (int i = 0; i <= 2; i++)
		DDA_Line(x[i], y[i], x[(i + 1) % 3], y[(i + 1) % 3], ren);
}
//
void DrawSquare(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[4];
	int y[4];
	float quay;
	quay = 0;
	for (int i = 0; i < 4; i++)
	{
		x[i] = xc + int(R*cos(quay) + 0.5);
		y[i] = yc - int(R*sin(quay) + 0.5);
		quay = quay + (2 * quay)	/ 4;
	}
	for (int i = 0; i < 4; i++)
	{
		SDL_RenderDrawPoint(ren, x[i], y[i]);
	}
	for (int i = 0; i < 4; i++)
		DDA_Line( x[i], y[i], x[(i + 1) % 4], y[(i + 1) % 4], ren);
}
//
void DrawPentagon(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5];
	int y[5];
	float quay = 3.14 / 2;
	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + int(R*cos(quay) + 0.5);
		y[i] = yc - int(R*sin(quay) + 0.5);
		quay = quay + (2 * 3.14) / 5;
	}
	for (int i = 0; i < 5; i++)
	{
		SDL_RenderDrawPoint(ren, x[i], y[i]);
	}
	for (int i = 0; i < 5; i++)
		DDA_Line(x[i], y[i], x[(i + 1) % 5], y[(i + 1) % 5], ren);
}
void DrawHexagon(int xc, int yc, int R, SDL_Renderer *ren)
{
	float quay = 3.14 / 2;
	int x[6];
	int y[6];
	for (int i = 0; i < 6; i++)
	{
		y[i] = yc - int(R*sin(quay) + 0.5);
		x[i] = xc + int(R*cos(quay) + 0.5);
		quay = quay + 2 * 3.14 / 6;
	}
	for (int i = 0; i < 6; i++)
		DDA_Line(x[i], y[i], x[(i + 1) % 6], y[(i + 1) % 6], ren);

	}
//
void DrawStar(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5];
	int y[5];
	float quay = 3.14 / 2;
	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + int(R*cos(quay) + 0.5);
		y[i] = yc - int(R*sin(quay) + 0.5);
		quay += 2 * 3.14 / 5;
	}
	for (int i = 0; i < 5; i++)
	{
		SDL_RenderDrawPoint(ren, x[i], y[i]);
	}
	for (int i = 0; i < 5; i++)
		DDA_Line(x[i], y[i], x[(i + 2) % 5], y[(i + 2) % 5], ren);
}
//
void DrawEmptyStar(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5], xp[5];
	int y[5], yp[5];
	float quay = 3.14 / 2;
	float r = R * sin(3.14 / 10) / sin(7 * 3.14 / 10);
	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + int(R*cos(quay) + 0.5);
		y[i] = yc - int(R*sin(quay) + 0.5);
		xp[i] = xc + int(r*cos(quay + 3.14 / 5) + 0.5);
		yp[i] = yc - int(r*sin(quay + 3.14 / 5) + 0.5);
		quay = quay + 2 * 3.14 / 5;
	}

	for (int i = 0; i < 5; i++)
	{
		DDA_Line(x[i], y[i], xp[i], yp[i], ren);
		DDA_Line(xp[i], yp[i], x[(i + 1) % 5], y[(i + 1) % 5], ren);
	}
}
//
void DrawStarEight(int xc, int yc, int R, SDL_Renderer *ren)
{

	int x[8], xp[8];
	int y[8], yp[8];
	float quay;
	quay = 0;
	float r = R * sin(3.14 / 8) / sin(6 * 3.14 / 8);
	for (int i = 0; i < 8; i++)
	{
		x[i] = xc + int(R*cos(quay) + 0.5);
		y[i] = yc - int(R*sin(quay) + 0.5);
		xp[i] = xc + int(r*cos(quay + 3.14 / 8) + 0.5);
		yp[i] = yc - int(r*sin(quay + 3.14 / 8) + 0.5);
		quay = quay + 2 * 3.14 / 8;
	}

	for (int i = 0; i < 8; i++)
	{
		DDA_Line(x[i], y[i], xp[i], yp[i], ren);
		DDA_Line(xp[i], yp[i], x[(i + 1) % 8], y[(i + 1) % 8], ren);
	}
}
//
void DrawStarAngle(int xc, int yc, int R, float startAngle, SDL_Renderer *ren)
{
	float quay = startAngle;
	int x[5], xp[5];
	int y[5], yp[5];
	float r = R * sin(3.14 / 10) / sin(7 * 3.14 / 10);
	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + int(R*cos(quay) + 0.5);
		y[i] = yc - int(R*sin(quay) + 0.5);
		xp[i] = xc + int(r*cos(quay + 3.14 / 5) + 0.5);
		yp[i] = yc - int(r*sin(quay + 3.14 / 5) + 0.5);
		quay = quay + 2 * 3.14 / 5;
	}

	for (int i = 0; i < 5; i++)
	{
		DDA_Line(x[i], y[i], xp[i], yp[i], ren);
		DDA_Line(xp[i], yp[i], x[(i + 1) % 5], y[(i + 1) % 5], ren);
	}
}
//
void DrawConvergentStar(int xc, int yc, int r, SDL_Renderer *ren)
{
	float quay = 3.14 / 2;
	int bk = r;
	while (bk > 1) 
	{
		DrawStarAngle(xc, yc, int(bk + 0.5),quay, ren);
		bk = bk * sin(3.14 / 10) / sin(7 * 3.14 / 10);
		quay = quay + 3.14;
	}
}